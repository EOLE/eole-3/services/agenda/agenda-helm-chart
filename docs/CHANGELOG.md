# Changelog

### [1.6.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/compare/release/1.6.0...release/1.6.1) (2024-10-07)


### Bug Fixes

* deploy agenda 3.14.0 ([04ad9d0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/commit/04ad9d0d73f74b180ae95e932b602857e65631dd))

## [1.6.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/compare/release/1.5.0...release/1.6.0) (2024-06-12)


### Features

* update appVersion to 3.13.0 ([8b5877a](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/commit/8b5877ab91fd2b2ebbdf1756ff42ce5c72555378))


### Bug Fixes

* restore testing tag ([142ee58](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/commit/142ee58a02bf9fd02e573869a6098a1cd19558d7))
* wait 5 minutes before killing pod at startup ([4a98cbe](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/commit/4a98cbeb933aa4a0dd49a8330a5e44d188c236bf))

## [1.5.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/compare/release/1.4.0...release/1.5.0) (2024-01-16)


### Features

* update app version to 3.12.0 ([827437c](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/commit/827437cdad4b0757234731de6d68f63bc1f1385d))

## [1.4.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/compare/release/1.3.0...release/1.4.0) (2023-10-17)


### Features

* new stable helm version for app version 3.11.0 ([4e71a35](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/commit/4e71a3513160d9e4c707ae1cd09fe6227e912607))

## [1.3.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/compare/release/1.2.1...release/1.3.0) (2023-08-25)


### Features

* new stable version for 3.10.0 ([677a899](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/commit/677a8995ac5d0653f324c38a4e8ac14bb45399b3))

### [1.2.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/compare/release/1.2.0...release/1.2.1) (2023-05-09)


### Bug Fixes

* dev chart version deploy dev app version ([fcd4ff0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/commit/fcd4ff000a5d7704a681972a21ff015de6894fa5))
* new helm stable version ([76d2920](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/commit/76d2920a41b5e117d7be5dde662ba0837c9423d2))
* testing helm deploy testing app version ([ec48fe5](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/commit/ec48fe50a76f58171f73b6f43ca1e24483995c5b))

## [1.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/compare/release/1.1.0...release/1.2.0) (2023-04-18)


### Features

* publish stable version for agenda 3.9.0 ([6f43cfa](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/agenda-helm-chart/commit/6f43cfa9bfaf52748e12e70e25ece6791d950f34))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/compare/release/1.0.0...release/1.1.0) (2023-02-01)


### Features

* publish agenda 3.8.0 ([d822313](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/commit/d822313c5f3f88dc2ad456deba752c206bb2510d))

## 1.0.0 (2022-12-08)


### Features

* **ci:** build helm package ([fa09f0c](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/commit/fa09f0c9412711924abe699a46fa7d7911fb33e7))
* **ci:** push helm package to chart repository ([6e0520c](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/commit/6e0520c462d665aa053b16be7e20116ac30e3d93))
* **ci:** update helm chart version with `semantic-release` ([f85116b](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/commit/f85116b40e9d25b14c815582eccab142b61f7685))
* **ci:** validate helm chart at `lint` stage ([0c60245](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/commit/0c602458dab753158ec633d2489eeedba71fe0ff))
* update app version to 3.7.0 ([c243ade](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/commit/c243adecdb0a09b78794d0a0482d84b6f2665422))
* use chart.yaml as default version ([0c48fdb](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/commit/0c48fdb5656e2d18937adf41f8008245c6ab4677))


### Bug Fixes

* **agenda:** add containerport default value ([bce7dc6](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/commit/bce7dc693b3fcafe9170ca4be9999bb332c8e542))
* **agenda:** add image repository default value ([9756d88](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/commit/9756d88dce1d394b27e9fd787c77321d21a46611))
* update hpa api version removed on k8s 1.26 ([9090b7a](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/commit/9090b7adb062ebe7d20d7ee77176a3e6bae60e0f))


### Continuous Integration

* **commitlint:** enforce commit message format ([85f3a94](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/commit/85f3a940dc4fa28ba7197e22aa0ac40945c34600))
* **release:** create release automatically with `semantic-release` ([29fe100](https://gitlab.mim-libre.fr/EOLE/eole-3/services/agenda/commit/29fe100018346757403f8633d06a9f7cca3df819))
